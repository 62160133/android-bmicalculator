package com.example.bmicalculator

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.example.bmicalculator.databinding.ActivityMainBinding
import java.text.NumberFormat

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calculateButton.setOnClickListener { calculateBMI() }
        binding.heightEditText.setOnKeyListener { view, keyCode, _ -> handleKeyEvent(view, keyCode)
        }
        binding.weightEditText.setOnKeyListener { view, keyCode, _ -> handleKeyEvent(view, keyCode)
        }
    }

    private fun displayBMI(bmi: Double, result: String) {
        val formattedBmi = NumberFormat.getCurrencyInstance().format(bmi)
        binding.bmiAmount.text = getString(R.string.bmi_amount, formattedBmi)
        binding.result.text = result
    }

    private fun calculateBMI() {
        val stringInHeightTextField = binding.heightEditText.text.toString()
        val height = stringInHeightTextField.toDoubleOrNull()
        val stringInWeightTextField = binding.weightEditText.text.toString()
        val weight = stringInWeightTextField.toDoubleOrNull()
        if(weight == null || weight == 0.0 || height == null || height == 0.0) {
            displayBMI(0.0, "")
            return
        }
        val bmi = weight / ((height/100) * (height/100))
        val result = if (bmi > 0 && bmi <= 18.4){
            "Underweight"
        }else if(bmi > 18.5 && bmi <= 24.9){
            "Normal weight"
        }else if(bmi > 25.0 && bmi <= 29.9){
            "Overweight"
        }else if(bmi > 30.0 && bmi <= 39.9){
            "Obesity"
        }else{
            "Extremely Obesity"
        }
        displayBMI(bmi, result)
    }

    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }

}